<?php

/**
 * Class ShapeOptions
 */
class ShapeOptions {

	public  $radius;
	public  $side_one;
	public  $side_two;

	public function setRadius( $radius ) {
		$this->radius = $radius;
	}

	public function setSideOne( $side_one ) {
		$this->radius = $side_one;
	}

	public function setSideTwo( $side_two ) {
		$this->side_two = $side_two;
	}

	public function getRadius() {
		return $this->radius;
	}

	public function getSideOne() {
		return $this->side_one;
	}

	public function getSideTwo() {
		return $this->side_two;
	}

}