<?php

/**
 * Class Rectangle
 */
class Rectangle extends AbstractShape {

	private $side_one;
	private $side_two;

	public function __construct( ShapeOptions $shapeOptions ) {
		$this->side_one = $shapeOptions->side_one;
		$this->side_two = $shapeOptions->side_two;
	}

	/**
	 * Calculate area using formula: a * b
	 *
	 * @return number
	 */
	public function area() {
		return $this->side_one * $this->side_two;
	}

	/**
	 * Calculate perimeter using formula: 2(a + b)
	 *
	 * @return int
	 */
	public function perimeter() {
		return $this->side_one * 2 + $this->side_two * 2;
	}

	public function scale( $direction, $scale ) {
		if ( $direction == 'up' ) {
			$this->side_one = $this->side_one + ( $this->side_one * $scale );
			$this->side_two = $this->side_two + ( $this->side_two * $scale );
		} else {
			$this->side_one = $this->side_one - ( $this->side_one * $scale );
			$this->side_two = $this->side_two - ( $this->side_two * $scale );
		}
	}
}
