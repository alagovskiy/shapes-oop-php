<?php

/**
 * Class AbstractShape
 */
abstract class AbstractShape {

	public abstract function area();

	public abstract function perimeter();

	public abstract function scale( $direction, $scale );

}