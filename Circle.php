<?php

/**
 * Class Circle
 */
class Circle extends AbstractShape {

	public $radius;

	public function __construct( ShapeOptions $shapeOptions ) {
		$this->radius = $shapeOptions->radius;
	}

	/**
	 * Calculate area using formula: R^2 * PI
	 *
	 * @return number
	 */
	public function area() {
		return pow( $this->radius, 2 ) * M_PI;
	}

	/**
	 * Calculate perimeter using formula: 2PI * R
	 *
	 * @return int
	 */
	public function perimeter() {
		return 2 * M_PI * $this->radius;
	}

	public function scale( $direction, $scale ) {
		if ( $direction == 'up' ) {
			$this->radius = $this->radius + ( $this->radius * $scale );
		} else {
			$this->radius = $this->radius - ( $this->radius * $scale );
		}
	}
}