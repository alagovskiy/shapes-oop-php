<?php

/**
 * Class Square
 */
class Square extends AbstractShape {

	private $side;

	public function __construct( ShapeOptions $shapeOptions ) {
		$this->side = $shapeOptions->side_one;
	}

	/**
	 * Calculate area using formula: a^2
	 *
	 * @return number
	 */
	public function area() {
		return pow( $this->side, 2 );
	}

	/**
	 * Calculate perimeter using formula: 4 * a
	 *
	 * @return int
	 */
	public function perimeter() {
		return $this->side * 4;
	}

	public function scale( $direction, $scale ) {
		if ( $direction == 'up' ) {
			$this->side = $this->side + ( $this->side * $scale );
		} else {
			$this->side = $this->side - ( $this->side * $scale );
		}
	}
}