<?php

/**
 * Class RightTriangle
 */
class RightTriangle extends AbstractShape {

	private $side_one;
	private $side_two;

	public function __construct( ShapeOptions $shapeOptions ) {
    $this->side_one = $shapeOptions->side_one;
    $this->side_two = $shapeOptions->side_two;
	}

	/**
	 * Calculate area using formula: 0.5ab
	 *
	 * @return number
	 */
	public function area() {
		return ( $this->side_one * $this->side_two / 2 );
	}

	/**
	 * Calculate perimeter using formula: a + b + sqrt(a^2 + b^2)
	 *
	 * @return int
	 */
	public function perimeter() {
		return $this->side_one + $this->side_two + sqrt( pow( $this->side_one, 2 ) + pow( $this->side_two, 2 ) );
	}

	public function scale( $direction, $scale ) {
		if ( $direction == 'up' ) {
			$this->side_one = $this->side_one + ( $this->side_one * $scale );
			$this->side_two = $this->side_two + ( $this->side_two * $scale );
		} else {
			$this->side_one = $this->side_one - ( $this->side_one * $scale );
			$this->side_two = $this->side_two + ( $this->side_two * $scale );
		}
	}
}