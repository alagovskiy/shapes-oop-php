<?php

/**
 * Class EquilateralTriangle
 */
class EquilateralTriangle extends AbstractShape {

	public $side;

	public function __construct( ShapeOptions $shapeOptions ) {
		$this->side = $shapeOptions->side_one;
	}

	/**
	 * Calculate area using formula: ( sqrt(3) / 4 ) * a^2
	 *
	 * @return number
	 */
	public function area() {
		return ( sqrt( 3 ) / 4 ) * pow( $this->side, 2 );
	}

	/**
	 * Calculate perimeter using formula: a + a + a
	 *
	 * @return int
	 */
	public function perimeter() {
		return 3 * $this->side;
	}

	public function scale( $direction, $scale ) {
		if ( $direction == 'up' ) {
			$this->side = $this->side + ( $this->side * $scale );
		} else {
			$this->side = $this->side - ( $this->side * $scale );
		}
	}
}
