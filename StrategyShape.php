<?php

/**
 * Class StrategyShape
 */
class StrategyShape {

	public $strategy_shape = NULL;

	public function create( $shape_name = '', ShapeOptions $shape_param ) {

		if ( !empty( $shape_name ) && class_exists( $shape_name ) ) {
			spl_autoload_register( function ( $shape_name ) {
				include 'AbstractShape.php';
				include 'StrategyShape.php';
				include 'ShapeOptions.php';

				include $shape_name . '.php';
			} );
			try {
				$this->strategy_shape = new $shape_name( $shape_param );
			} catch ( Exception $e ) {
				echo $e->getMessage(), "\n";
			}

		}
	}

}
