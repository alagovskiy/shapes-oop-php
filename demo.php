<?php

echo 'BEGIN TEST';

$radius   = 2;
$side_one = 4;
$side_two = 5;

$shape = new ShapeOptions();
$shape->setRadius( 2 );
$shape->setSideOne( $side_one );
$shape->setSideTwo( $side_two );

$strategy = ( new StrategyShape() )->create( 'Circle', $shape );

echo $strategy->strategy_shape->area();

echo 'END TEST';
