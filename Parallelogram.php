<?php

/**
 * Class Parallelogram
 */
class Parallelogram extends AbstractShape {

	public $side_one;
	public $side_two;
	public $height;

	public function __construct( ShapeOptions $shapeOptions ) {
		$this->side_one = $shapeOptions->side_one;
		$this->side_two = $shapeOptions->side_two;
		$this->height   = $shapeOptions->side_one / $shapeOptions->side_two;
	}

	/**
	 * Calculate area using formula: b * h
	 *
	 * @return number
	 */
	public function area() {
		return $this->side_two * $this->height;
	}

	/**
	 * Calculate perimeter using formula: side + side + side
	 *
	 * @return int
	 */
	public function perimeter() {
		return 2 * ( $this->side_one + $this->side_two );
	}

	public function scale( $direction, $scale ) {
		if ( $direction == 'up' ) {
			$this->side_one = $this->side_one + ( $this->side_one * $scale );
			$this->side_two = $this->side_two + ( $this->side_two * $scale );
		} else {
			$this->side_one = $this->side_one - ( $this->side_one * $scale );
			$this->side_two = $this->side_two - ( $this->side_two * $scale );
		}
	}
}